jQuery(document).ready(function($) {
  var width = $(window).width();

  if ($("#wpadminbar").length) {
    fixMargin();
    $(document).scroll(function() {
      fixMargin();
    });
    $(window).resize(function() {
      width = $(window).width();
      fixMargin();
    });
  }

  function fixMargin() {
    if (width < 601) {
      mobileMargin();
    } else {
      $("header").css("margin-top", $("#wpadminbar").height());
    }
  }

  function mobileMargin() {
    if ($("#wpadminbar").height() - $(this).scrollTop() > 0) {
      $("header").css(
        "margin-top",
        $("#wpadminbar").height() - $(this).scrollTop()
      );
    } else {
      $("header").css("margin-top", 0);
    }
  }
});
