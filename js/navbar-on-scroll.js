jQuery(document).ready(function($) {
  var $nav = $("header");
  var height = $nav.height();

  if ($(window).width() < 768) {
    toggleClass(2.75);
  } else {
    toggleClass(1.5);
  }

  $(document).scroll(function() {
    if ($(window).width() < 768) {
      toggleClass(2.75);
    } else {
      toggleClass(1.5);
    }
  });

  function toggleClass(factor) {
    $nav.toggleClass("scrolled", $(this).scrollTop() > height * factor);
  }
});
