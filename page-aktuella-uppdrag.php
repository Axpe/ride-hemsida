<?php get_header();?>
<main class="container">
  <?php get_template_part( 'template-parts/uppdrags-navigator', 'page' ); ?>

  <?php
      $jobs = get_posts([
          'post_type' => 'pnty_job',
          'numberposts' => -1,
      ]);
  ?>
  <?php if (count($jobs) > 0):?>
      <ul class="current-ads">
          <?php foreach($jobs as $job):?>
              <li class="ponty-job">
                  <a
                    class="overlay-link"
                     href="<?php echo get_permalink($job->ID);?>"
                     title="Permalänk till <?php echo $job->post_title;?>"
                  >
                  </a>
                  <?php if (metadata_exists('post', $job->ID, '_pnty_logo')): ?>
                    <img alt="job image" class="ponty-img" src="<?php echo get_post_meta( $job->ID, '_pnty_logo', true );?>" />
                  <?php endif ?>
                  <div class="ponty-content">
                    <h3 class="ponty-job-title">
                      <?php echo $job->post_title;?>
                    </h3>
                    <span>
                      <?php echo get_post_meta($job->ID, '_pnty_organization_name', true); ?>
                    </span>
                    <div class="ponty-location">
                      <?php if (metadata_exists('post', $job->ID, '_pnty_location')): ?>
                        <span>
                          <?php echo get_post_meta($job->ID, '_pnty_location', true); ?>
                        </span>
                      <?php endif ?>
                      <?php if (metadata_exists('post', $job->ID, '_pnty_region')): ?>
                        <span>
                          <?php echo get_post_meta($job->ID, '_pnty_region', true); ?>
                        </span>
                      <?php endif ?>
                  </div>
                  </div>
              </li>
          <?php endforeach;?>
      </ul>
  <?php else:?>
      <p>Det finns inga publicerade annonser just nu.</p>
  <?php endif;?>
</main>
<?php get_footer(); ?>
