<?php get_header(); ?>
<main>
    <div class="container col-xs-12 subpage-content">
      <?php
  		if ( have_posts() ) {
  			// Load posts loop.
  			while ( have_posts() ) {
  				the_post();
          the_content();
  			}
  		}
  		?>
    </div>
  </main>
<?php get_footer(); ?>
