
  <?php
   wp_nav_menu([
     'theme_location'  => 'missions',
     'container_class'   => 'uppdrags-navigator',
     'menu_class'      => 'nav nav-tabs flex-nowrap text-center',
     'add_li_class'  => 'nav-item',
     'fallback_cb'     => 'bs4navwalker::fallback',
     'walker'          => new bs4navwalker()
   ]);
   ?>
