<?php get_header();?>
  <main class="container">
    <?php if (have_posts()): while(have_posts()): the_post();?>
    <div class="pnty-job">
        <?php if (metadata_exists('post', get_the_ID(), '_pnty_logo')): ?>
          <div class="img-container">
            <img class="ponty-img" src="<?php echo get_post_meta( get_the_ID(), '_pnty_logo', true );?>" />
          </div>
        <?php endif ?>
        <div class="ponty-content">
          <?php echo get_the_content();?>
        </div>
        <div class="ponty-button">
          <?php echo get_post_meta( get_the_ID(), '_pnty_apply_btn', true );?>
        </div>
    </div>
<?php endwhile; endif;?>
  </main>
<?php get_footer(); ?>
