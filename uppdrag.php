<?php
/*
 Template Name: Uppdrag
 */
 get_header();?>
  <main>
    <div class="container">
      <?php get_template_part( 'template-parts/uppdrags-navigator', 'page' ); ?>

      <?php
  		if ( have_posts() ) {
  			// Load posts loop.
  			while ( have_posts() ) {
  				the_post();
          the_content();
  			}
  		}
  		?>
    </div>
  </main>
<?php get_footer(); ?>
