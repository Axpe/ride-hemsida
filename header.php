<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header class="position-fixed text-light">
	<div class="container-fluid">
		<nav class="navbar-expand-lg justify-content-between">
		 <div class="desktop-menu d-none d-lg-flex align-items-center">
			 <div class="logo d-flex justify-content-start h-100">
				 <a class="banner-desktop" href="<?php echo get_site_url(); ?>">
					 <img alt="Website logo" class="px-1 py-2" src="<?php echo get_header_image(); ?>">
				 </a>
			 </div>
			 <div id="desktop-nav" class="navbar-list d-flex justify-content-center">
			 	<?php
					wp_nav_menu([
						'theme_location'  => 'menu-1',
						'container'		  => false,
						'menu_id'         => "desktop-primary-menu",
						'menu_class'      => 'navbar-nav text-center justify-content-center',
						'depth'           => 2,
						'fallback_cb'     => 'bs4navwalker::fallback',
						'walker'          => new bs4navwalker()
					]);
				?>
			</div>
			<div class="social-media d-flex justify-content-end h-100 align-items-center">
				<?php if( get_field( "facebook_lank" , get_option( 'page_on_front' ))): ?>
					<a href="<?php the_field("facebook_lank", get_option( 'page_on_front' ));?>" target="_blank"><span class="icon-facebook-squared"></span></a>
				<?php endif; ?>
				<?php if( get_field("instagram_lank", get_option(( 'page_on_front' )))): ?>
					<a href="<?php the_field("instagram_lank", get_option( 'page_on_front' ));?>" target="_blank"><span class="icon-instagram"></span></a>
				<?php endif; ?>
				<?php if( get_field("linkedin_lank", get_option(("page_on_front")))): ?>
					<a href="<?php the_field("linkedin_lank", get_option( 'page_on_front' ));?>" target="_blank"><span class="icon-linkedin-squared"> </a>
				<?php endif; ?>
			</div>
		</div>
		 <div class="mobile-menu d-lg-none">
			 <a class="banner-mobile" href="<?php echo get_site_url(); ?>">
					<img alt="Website logo" class="p-3" src="<?php echo get_header_image(); ?>">
				</a>
			 <input type="checkbox" id="show-menu" role="button">
			 <label for="show-menu" class="show-menu">
				 <i class="show-menu-icon">
					 <span class="show-menu-icon-bar">
					 </span>
				 </i>
			 </label>
			<div id="mobile-nav" class="navbar-list">
				<?php
					wp_nav_menu([
						'theme_location'  => 'menu-1',
						'menu_id'         => "mobile-primary-menu",
						'container'       => false,
						'menu_class'      => 'navbar-nav mr-auto',
						'depth'           => 2,
						'fallback_cb'     => 'bs4navwalker::fallback',
						'walker'          => new bs4navwalker()
					]);
					?>
			</div>
				<div class="social-media">
					<?php if( get_field( "facebook_lank" , get_option( 'page_on_front' ))): ?>
						<a href="<?php the_field("facebook_lank", get_option( 'page_on_front' ));?>" target="_blank"><span class="icon-facebook-squared"></span></a>
					<?php endif; ?>
					<?php if( get_field("instagram_lank", get_option(( 'page_on_front' )))): ?>
						<a href="<?php the_field("instagram_lank", get_option( 'page_on_front' ));?>" target="_blank"><span class="icon-instagram"></span></a>
					<?php endif; ?>
					<?php if( get_field("linkedin_lank", get_option(("page_on_front")))): ?>
						<a href="<?php the_field("linkedin_lank", get_option( 'page_on_front' ));?>" target="_blank"><span class="icon-linkedin-squared"> </a>
					<?php endif; ?>
				</div>
		 </div>
		</nav>
	</div>
</header>
<?php if(!is_front_page()): ?>
	<div id="banner" class="title-block d-flex flex-column bg-dark text-light">
		<h1 class="page-title">
			<?php echo get_the_title(); ?>
		</h1>
		<?php if( get_field('subtitle') ): ?>
			<h2 class="page-subtitle"><?php the_field('subtitle'); ?></h2>
		<?php endif; ?>
	</div>
<?php endif; ?>
