<?php get_header();?>
<main>
  <div class="container col-xs-12">
    <div class="button-container d-flex align-items-center mb-5 flex-column">
      <h5><?php the_field("vad_vill_du_veta_mer_om")?></h5>
      <div class="contact-button">
        <a id="show-form">JAG VILL HA HJÄLP ATT REKRYTERA</a>
      </div>
      <div class="contact-form mt-3" id="contact-form" style="display: none">
        <?php echo do_shortcode('[contact-form-7 id="1429" title="Kontaktformulär"]'); ?>
      </div>
      <?php
      $recruit = get_field('rekryteringslank');
      if ($recruit):?>
        <div class="contact-button">
          <a href="<?php echo $recruit["url"]?>" target="<?php echo $recruit["target"]?>"><?php echo $recruit["title"]?></a>
        </div>
      <?php endif; ?>
    </div>
    <div class="profiles row mb-5">
      <?php
      $profiles = get_field("personer");
      if($profiles):
      ?>
        <?php foreach($profiles as $profile): ?>
          <div class="profile col-md-6 p-0">
            <div class="card h-100 border-0">
              <div class="card-img-top profile-image d-flex justify-content-center">
                <?php echo get_wp_user_avatar($profile["ID"], "large") ?>
              </div>
              <div class="profile-info card-body text-dark text-center">
                <h4><?php echo $profile["display_name"]?></h4>
                <p class="mb-2"><?php echo $profile["user_description"]?></p>
                <h5><a aria-label="Phone number" href="tel:<?php echo get_user_meta($profile["ID"], 'ext_phone' , true) ?>"><?php echo get_user_meta($profile["ID"], 'ext_phone' , true) ?></a></h5>
                <h5><a aria-label="Mail address" href="mailto:<?php echo $profile["user_email"] ?>"><?php echo $profile["user_email"] ?></a></h5>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
    <?php endif; ?>
    </div>
  </div>

  <div class="map">
    <iframe src=<?php the_field('karta'); ?> width="100%" height="600" frameborder="0" style="border:0" allowfullscreen title="Google map"></iframe>
  </div>
</main>


<?php get_footer(); ?>
