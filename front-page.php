<?php get_header() ?>
<div class="d-flex flex-column bg-dark text-light text-center align-items-center title-block" id="front-page-title-banner" style='background-image: url(<?php echo the_field("bakgrundsbild") ?>)'>
  <div class="jumbotron-container container-fluid">
    <div class="jumbotron-block">
      <h1 class="tagline mb-4"><?php the_field("title") ?></h1>
      <?php if( get_field('subtitle') ): ?>
        <h4 class="tagline-subtitle  mb-4"><?php the_field("subtitle") ?></h4>
      <?php endif; ?>
      <?php
      $links = get_field("links");
      if($links):
      ?>
        <div class="links">
          <?php foreach($links as $link): ?>
            <div class="title-link">
              <a href=<?php echo $link; ?>><?php echo get_the_title(url_to_postid($link));?></a>
            </div>
          <?php endforeach ?>
        </div>
      <?php endif; ?>
    </div>
    <?php
      $partners = acf_photo_gallery('partners', $post->ID);
      if($partners):
    ?>
      <div class="partners-container container">
        <div class="partners row">
          <?php

              foreach($partners as $partner):
          ?>
            <?php $thumbnailURL =  $partner['full_image_url'];
            $title = $partner['title'];
            echo $thumbnail;
            ?>
            <div class="thumbnail col-md-3 col-4">
              <img src=<?php echo $thumbnailURL ?> alt=<?php echo $title ?> >
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    <?php endif;?>
  </div>
</div>
<main class="front-page-content">
  <div class="container col-xs-12 pt-5 pb-5">
    <div class="mb-3">

      <?php
      if ( have_posts() ) {
        // Load posts loop.
        while ( have_posts() ) {
          the_post();
          the_content();
        }
      }
      ?>
    </div>
    <div class="pt-1">
      <?php
        // query for the about page
        $your_query = new WP_Query( 'pagename=vart-erbjudande' );
        // "loop" through query (even though it's just one page) 
        while ( $your_query->have_posts() ) : $your_query->the_post();
            the_content();
        endwhile;
        // reset post data (important!)
        wp_reset_postdata();
      ?>
    </div>
  </div>
</main>

<?php get_footer() ?>
