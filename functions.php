<?php


if ( ! function_exists( 'ride_setup' ) ) :
  /**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function ride_setup() {
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );


		/* Set custom header */
		$args = array(
			'flex-width'    => true,
			'width'         => 980,
			'flex-height'    => true,
			'height'        => 200,
			'default-image' => get_template_directory_uri() . '/images/logo-header.png',
		);
		add_theme_support( 'custom-header', $args );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'menu-1' => __( 'Primär', 'ride' ),
				'footer' => __( 'Footer Meny', 'ride' ),
				'social' => __( 'Sociala länkar', 'ride' ),
				'missions' => __('Uppdragsmenu', 'ride'),
				'om-oss' => __('Om oss-menu', 'ride')
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 190,
				'width'       => 190,
				'flex-width'  => false,
				'flex-height' => false,
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for Block Styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for editor styles.
		add_theme_support( 'editor-styles' );

		// Enqueue editor styles.
		add_editor_style( 'style-editor.css' );

		// Add custom editor font sizes.
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => __( 'Small', 'ride' ),
					'shortName' => __( 'S', 'ride' ),
					'size'      => 19.5,
					'slug'      => 'small',
				),
				array(
					'name'      => __( 'Normal', 'ride' ),
					'shortName' => __( 'M', 'ride' ),
					'size'      => 22,
					'slug'      => 'normal',
				),
				array(
					'name'      => __( 'Large', 'ride' ),
					'shortName' => __( 'L', 'ride' ),
					'size'      => 36.5,
					'slug'      => 'large',
				),
				array(
					'name'      => __( 'Huge', 'ride' ),
					'shortName' => __( 'XL', 'ride' ),
					'size'      => 49.5,
					'slug'      => 'huge',
				),
			)
		);

	}
endif;
add_action( 'after_setup_theme', 'ride_setup' );

/*
* Telefonnummer
*/

add_filter('user_contactmethods', 'custom_user_contactmethods');
function custom_user_contactmethods($user_contact){
  $user_contact['ext_phone'] = 'Phone number';

  return $user_contact;
}

/*
* Add bootstrap navwalker
*/
require_once('bs4navwalker.php');

/* add_li_class for menus */

function add_additional_class_on_li($classes, $item, $args) {
    if($args->add_li_class) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ride_widgets_init() {

	register_sidebar(
		array(
			'name'          => __( 'Footer-1', 'ride' ),
			'id'            => 'footer-1',
			'description'   => __( 'Add widgets here to appear in your footer.', 'ride' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h5 class="widget-title">',
			'after_title'   => '</h5>',
		)
	);

	register_sidebar(
		array(
			'name'          => __( 'Footer-2', 'ride' ),
			'id'            => 'footer-2',
			'description'   => __( 'Add widgets here to appear in your footer.', 'ride' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h5 class="widget-title">',
			'after_title'   => '</h5>',
		)
	);
	register_sidebar(
		array(
			'name'          => __( 'Footer-3', 'ride' ),
			'id'            => 'footer-3',
			'description'   => __( 'Add widgets here to appear in your footer.', 'ride' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h5 class="widget-title">',
			'after_title'   => '</h5>',
		)
	);

}
add_action( 'widgets_init', 'ride_widgets_init' );

// include custom jQuery
function shapeSpace_include_custom_jquery() {

	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), null, true);

}
add_action('wp_enqueue_scripts', 'shapeSpace_include_custom_jquery');


/**
 * Enqueue scripts and styles.
 */
function ride_scripts() {
	wp_enqueue_style( 'ride-style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );
	wp_enqueue_script( 'navbar-on-scroll', get_template_directory_uri() . '/js/navbar-on-scroll.js', "", "", true);
	wp_enqueue_script( 'admin-bar-margin', get_template_directory_uri() . '/js/admin-bar-margin.js', "", "", true);
	wp_enqueue_script( 'show-contact-form', get_template_directory_uri() . '/js/show-contact-form.js', "", "", true);
}
add_action( 'wp_enqueue_scripts', 'ride_scripts' );


?>
