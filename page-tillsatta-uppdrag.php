<?php get_header();?>
<main class="container">
  <?php get_template_part( 'template-parts/uppdrags-navigator', 'page' ); ?>

  <?php
      $jobs = get_posts([
          'post_type' => 'pnty_job_showcase',
          'numberposts' => -1,
      ]);
  ?>
  <?php if (count($jobs) > 0):?>
      <ul class="current-ads">
          <?php foreach($jobs as $job):?>
              <li class="ponty-job">
                  <?php if (metadata_exists('post', $job->ID, '_pnty_logo')): ?>
                    <img alt="job image" class="ponty-img" src="<?php echo get_post_meta( $job->ID, '_pnty_logo', true );?>" />
                  <?php endif ?>
                  <div class="ponty-content">
                    <h3 class="ponty-job-title">
                      <?php echo $job->post_title;?>
                    </h3>
                    <span>
                      <?php echo get_post_meta($job->ID, '_pnty_organization_name', true); echo ' - ' ?>
                    </span>
                    <span>
                      <?php echo get_post_meta($job->ID, '_pnty_location', true); ?>
                    </span>
                  </div>
              </li>
          <?php endforeach;?>
      </ul>
  <?php else:?>
      <p>Det finns inga publicerade annonser just nu.</p>
  <?php endif;?>
</main>
<?php get_footer(); ?>
