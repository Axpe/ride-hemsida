<?php get_header();?>
<?php $nav_items = wp_get_nav_menu_items("Om oss-menu");
  foreach($nav_items as $item):
    if($item->title == get_the_title()):?>
      <?php
        wp_nav_menu([
          'theme_location'  => 'om-oss',
          'menu_id'         => "secondary-menu",
          'container'       => 'div',
          "container_id" 		=> 	"",
          'container_class' => 'bg-dark',
          'menu_class'      => '',
          'depth'           => 2,
        ]);
      ?>
    <?php endif;
  endforeach?>
  <main>
    <div class="container col-xs-12 subpage-content">
      <?php
  		if ( have_posts() ) {
  			// Load posts loop.
  			while ( have_posts() ) {
  				the_post();
          the_content();
  			}
  		}
  		?>
    </div>
  </main>
<?php get_footer(); ?>
