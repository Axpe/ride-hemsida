<footer class="bg-dark text-light">
  <div class="container">
    <div class="row text-center footer-widget">
      <div class="col-md-4 my-5"><?php
        if(is_active_sidebar('footer-1')){
          dynamic_sidebar('footer-1');
        }
        ?>
      </div>
      <div class="col-md-4 my-5">
        <?php
          if(is_active_sidebar('footer-2')){
            dynamic_sidebar('footer-2');
          }
        ?>
      </div>
      <div class="col-md-4 my-5">
        <?php
          if(is_active_sidebar('footer-3')){
            dynamic_sidebar('footer-3');
          }
        ?>
      </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
