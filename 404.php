<?php get_header() ?>

<main>
    <div class="container text-center">
        <h1 class="display-3">Error 404</h1>
        <h5 class="lead">Sidan du letade efter kunde inte hittas. :(</h5>
    </div>
</main>
<?php get_footer() ?>
